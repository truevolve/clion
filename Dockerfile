FROM ubuntu:18.04
MAINTAINER Steyn Geldenhuys "steyn@truevolve.technology"

USER root
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get install -y clang cmake curl g++ gcc wget gdb sudo git firefox && \
    apt-get clean && apt-get purge && apt-get autoremove

RUN mkdir -p /home/developer/ && \
    echo "developer:x:1000:1000:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:1000:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    adduser developer video && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown developer:developer -R /home/developer

USER developer
RUN curl https://download-cf.jetbrains.com/cpp/CLion-2019.2.5.tar.gz -o /home/developer/clion.tar.gz

RUN  mkdir /home/developer/clion && \
     tar -xvzf /home/developer/clion.tar.gz -C /home/developer/clion --strip-components 1 && \
     rm /home/developer/clion.tar.gz && \
     chmod +x /home/developer/clion/bin/clion.sh

USER developer
ENV HOME /home/developer
WORKDIR /home/developer
CMD cd /home/developer/clion/bin/ && ./clion.sh
